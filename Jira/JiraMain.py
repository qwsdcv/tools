# -*- coding: UTF-8 -*-

from jira import JIRA
import sys
import smtplib
from email.mime.text import MIMEText
from email.header import Header

LABEL___ = u'LABEL'


class Main:
    def __init__(self, JiraUsername, JiraToken):
        # login Jira
        self.auth_jira = JIRA(basic_auth=(
            JiraUsername, JiraToken), server='https://jira.xxxxxx.com')
        self._handler = []

    def getFilter(self, filterId):
        list = self.auth_jira.search_issues('filter=%s' % filterId)
        return list

    def handlerIssueByFilter(self, filterId):
        for issue in self.getFilter(filterId):
            _issue = self.auth_jira.issue(issue.key)
            for handler in self._handler:
                handler(self.auth_jira, _issue)

    def addIssueHandler(self, handler):
        self._handler.append(handler)


def ADD_LABEL(jira, _issue):
    if _issue.fields.labels.count(LABEL___) == 0:
        _issue.fields.labels.append(LABEL___)
        _issue.update(fields={"labels": _issue.fields.labels})


def ADD_WATCHER(jira, _issue):
    watchers = jira.watchers(_issue.key)
    if not watchers.isWatching:
        jira.add_watcher(_issue, 'username')
        _SUBSCRIBE_P1_P2(jira, _issue)


def _SUBSCRIBE_P1_P2(jira, issue):
    receiver = ['xxxxxxxxxxxxxxxx@126.com']
    try:
        pri = issue.fields.priority.name
        if pri.startswith('2-'):
            ticket_num = issue.key
            title = '[P2 Ticket] %s' % ticket_num
            body = 'P2 Ticket %s Came.\n Take Care of it at https://jira.xxxxxxxx.com/browse/%s' % (
                ticket_num, ticket_num)
            SEND_MAIL(receiver, title, body)
        elif pri.startswith('1-'):
            ticket_num = issue.key
            title = '!!!!!!![P1 Ticket] %s' % ticket_num
            body = '!!!!!!!! P1 Ticket %s Came.\n Take Care of it at https://jira.xxxxxxx.com/browse/%s' % (
                ticket_num, ticket_num)
            SEND_MAIL(receiver, title, body)
    except:
        print 'Exception'
        return


FROM = 'system@xxxxx.com'
MAIN_HOST = 'mail.xxxxx.corp'
To = 'TO'


def SEND_MAIL(who, subject, msg):
    MESSAGE = MIMEText(msg, 'plain', 'utf-8')
    MESSAGE['From'] = Header(To, 'utf-8')
    MESSAGE['To'] = Header(To, 'utf-8')
    MESSAGE['Subject'] = Header(subject, 'utf-8')
    smtpObj = smtplib.SMTP(MAIN_HOST)
    smtpObj.sendmail(FROM, who, MESSAGE.as_string(True))


if __name__ == "__main__":
    un = sys.argv[1]
    up = sys.argv[2]
    main = Main(un, up)
    main.addIssueHandler(ADD_WATCHER)
    main.handlerIssueByFilter(114854)
